// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/MenuManager/ZZMenuManagerComponent.h"
#include "Settings/ZZUISettings.h"
#include "UI/Menu/ZZMenuBase.h"
#include "Kismet/GameplayStatics.h"

UZZMenuManagerComponent::UZZMenuManagerComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}	

void UZZMenuManagerComponent::OpenMenu(EMenuIndex InMenuIndex)
{
	ReleaseMenu();
	if (InMenuIndex == EMenuIndex::None)
	{
		UE_LOG(LogTemp, Warning, TEXT("UZZMenuManagerComponent::OpenMenu MenuIndex == None"));
		return;
	}
	
	TSubclassOf<UZZMenuBase> MenuWidgetClass;
	if (!GetMenuClassByMenuIndex(InMenuIndex, MenuWidgetClass))
	{
		return;
	}

	APlayerController* PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (!PlayerController)
	{
		UE_LOG(LogTemp, Warning, TEXT("UZZMenuManagerComponent::OpenMenu - Missing player controller"));
		return;
	}

	UZZMenuBase* MenuWidgetCreated = Cast<UZZMenuBase>(CreateWidget(PlayerController, MenuWidgetClass));
	if (!MenuWidgetCreated)
	{
		UE_LOG(LogTemp, Warning, TEXT("UZZMenuManagerComponent::OpenMenu - Create widget failed"));
		return;
	}

	FOnMenuOpen OnMenuOpen;
	OnMenuOpen.BindUObject(this, &UZZMenuManagerComponent::OpenMenu);
	FOnMenuClose OnMenuClose;
	OnMenuClose.BindUObject(this, &UZZMenuManagerComponent::CloseMenu);
	MenuWidgetCreated->BindMenu(OnMenuOpen, OnMenuClose);
	MenuWidgetCreated->AddToViewport(0);
	SetMenuOpened(MenuWidgetCreated);
}
void UZZMenuManagerComponent::CloseMenu()
{
	ReleaseMenu();
}
void UZZMenuManagerComponent::ReleaseMenu()
{
	if (!GetMenuOpened())
	{
		return;
	}
	GetMenuOpened()->UnbindMenu();
	GetMenuOpened()->RemoveFromParent();
	SetMenuOpened(nullptr);
}

bool UZZMenuManagerComponent::GetMenuClassByMenuIndex(EMenuIndex InMenuIndex, TSubclassOf<UZZMenuBase>& InMenuWidgetClass)
{
	TSoftClassPtr<UZZMenuBase> MenuWidgetClass = *GetDefault<UZZUISettings>()->MenuWidgetClass.Find(InMenuIndex);
	if (!MenuWidgetClass || MenuWidgetClass.IsNull())
	{
		UE_LOG(LogTemp, Warning, TEXT("UZZMenuManagerComponent::GetMenuClassByMenuIndex MenuIndx not found"));
		return false;
	}
	InMenuWidgetClass = MenuWidgetClass.LoadSynchronous();
	return true;
}

UZZMenuBase* UZZMenuManagerComponent::GetMenuOpened() const
{
	return MenuOpened;
}
void UZZMenuManagerComponent::SetMenuOpened(UZZMenuBase* InMenuOpened)
{
	MenuOpened = InMenuOpened;
}
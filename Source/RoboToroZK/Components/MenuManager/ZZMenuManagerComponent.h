// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"

#include "UI/Menu/ZZMenuDefines.h"
#include "UI/Menu/ZZMenuBase.h"

#include "ZZMenuManagerComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ROBOTOROZK_API UZZMenuManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UZZMenuManagerComponent();

	UFUNCTION(BlueprintCallable, Category = "Menu Manager")
	void OpenMenu(EMenuIndex InMenuIndex);
	UFUNCTION(BlueprintCallable, Category = "Menu Manager")
	void CloseMenu();
	void ReleaseMenu();

	bool GetMenuClassByMenuIndex(EMenuIndex InMenuIndex, TSubclassOf<UZZMenuBase>& InMenuWidgetClass);


	UFUNCTION(BlueprintCallable, BlueprintPure, Category="Menu Manager")
	UZZMenuBase* GetMenuOpened() const;
	void SetMenuOpened(UZZMenuBase* InMenuOpened);

protected:
	UPROPERTY()
	UZZMenuBase* MenuOpened = nullptr;
};

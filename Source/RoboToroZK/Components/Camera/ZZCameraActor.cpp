// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/Camera/ZZCameraActor.h"

// Sets default values
AZZCameraActor::AZZCameraActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AZZCameraActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AZZCameraActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "ZZHUD.generated.h"

class UZZMenuManagerComponent;
/**
 * 
 */
UCLASS()
class ROBOTOROZK_API AZZHUD : public AHUD
{
	GENERATED_BODY()
public:
	AZZHUD();

	UFUNCTION(BlueprintCallable, BlueprintPure)
	UZZMenuManagerComponent* GetMenuManagerComponent();

protected:
	UPROPERTY(BlueprintReadOnly)
	UZZMenuManagerComponent* MenuManagerComponent = nullptr;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/Powerup/ZZPowerup.h"

// Sets default values
AZZPowerup::AZZPowerup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AZZPowerup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AZZPowerup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/Character/ZZCharacter.h"

// Sets default values
AZZCharacter::AZZCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned 
void AZZCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AZZCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AZZCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}


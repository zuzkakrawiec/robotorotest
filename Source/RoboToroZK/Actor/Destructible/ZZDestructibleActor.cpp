// Fill out your copyright notice in the Description page of Project Settings.


#include "Actor/Destructible/ZZDestructibleActor.h"

// Sets default values
AZZDestructibleActor::AZZDestructibleActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AZZDestructibleActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AZZDestructibleActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


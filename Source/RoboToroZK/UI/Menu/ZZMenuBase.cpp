// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Menu/ZZMenuBase.h"


void UZZMenuBase::NativeOnInitialized()
{
	MenuIndex = EMenuIndex::MainMenu;
}
void UZZMenuBase::BindMenu(FOnMenuOpen InOnMenuOpen, FOnMenuClose InOnMenuClose)
{
	OnMenuOpen = InOnMenuOpen;
	OnMenuClose = InOnMenuClose;
}
void UZZMenuBase::UnbindMenu()
{
	OnMenuOpen.Unbind();
	OnMenuClose.Unbind();
}
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "ZZMenuDefines.generated.h"

UENUM()
enum class EMenuIndex : uint8
{
	None,
	MainMenu
};
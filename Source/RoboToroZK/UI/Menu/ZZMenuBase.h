// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "ZZMenuDefines.h"
#include "ZZMenuBase.generated.h"

DECLARE_DELEGATE_OneParam(FOnMenuOpen, EMenuIndex InMenuIndex);
DECLARE_DELEGATE(FOnMenuClose);

UCLASS()
class ROBOTOROZK_API UZZMenuBase : public UUserWidget
{
	GENERATED_BODY()
public:
	void BindMenu(FOnMenuOpen InOnMenuOpen, FOnMenuClose InOnMenuClose);
	void UnbindMenu();
protected:
	virtual void NativeOnInitialized() override;

	FOnMenuOpen OnMenuOpen;
	FOnMenuClose OnMenuClose;

	UPROPERTY()
	EMenuIndex MenuIndex = EMenuIndex::None;
};

// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/Button/ZZButtonBase.h"

void UZZButtonBase::NativeConstruct()
{
	Super::NativeConstruct();
	Button->OnPressed.AddDynamic(this, &UZZButtonBase::OnButtonPressedCallback);
	Button->OnHovered.AddDynamic(this, &UZZButtonBase::OnButtonHoveredCallback);
	Button->OnUnhovered.AddDynamic(this, &UZZButtonBase::OnButtonUnhoveredCallback);
	ReflectData();
}
void UZZButtonBase::NativePreConstruct()
{
	Super::NativePreConstruct();
	ReflectData();
}


void UZZButtonBase::ReflectData()
{
	ButtonText->SetText(TextOnButton);
}
void UZZButtonBase::OnButtonPressedCallback()
{
	OnButtonPressed.Broadcast(this);
}
void UZZButtonBase::OnButtonHoveredCallback()
{
	OnButtonHovered.Broadcast(this);
}
void UZZButtonBase::OnButtonUnhoveredCallback()
{
	OnButtonUnhovered.Broadcast(this);
}